<?php

namespace App\Models;

use App\Services\PersonService;
use Carbon\Carbon;

class Person extends BaseModel
{
    protected $table = 'persons';
    protected $primaryKey = 'id';
    protected $appends = [
        'id_update'
    ];
    protected $guarded = [];

    const MAX_AGE_DATA = 7;

    public function homeworld()
    {
        return $this->belongsTo(Homeworld::class);
    }

    public function vehicles()
    {
        return $this->hasMany(PersonVehicle::class);
    }
    
    public function starships()
    {
        return $this->hasMany(PersonStarship::class);
    }

    public function getIdUpdateAttribute()
    {
        $baseUrl = env('SWAPI_BASE_URL');
        $now = Carbon::now();
        $lastUpdate = Carbon::parse($this->updated_at);
        $dateDiff = $lastUpdate->diffInDays($now);
        if ((int)$dateDiff > (int)self::MAX_AGE_DATA) {
            $personService = new PersonService();
            $person = $personService->createRequest($baseUrl.'/people/'.$this->id);

            $personService->insertPerson($person);
            $personService->insertHomeworld($person->homeworld);
            $personService->insertVehicle($person->vehicles, $person->url);
            $personService->insertStarShips($person->starships, $person->url);

            return;
        }

        return;
    }
}
