<?php

namespace App\Models;

class PersonVehicle extends BaseModel
{
    protected $guarded = [];

    public function vehicle()
    {
        return $this->belongsTo(Vehicle::class);
    }
}
