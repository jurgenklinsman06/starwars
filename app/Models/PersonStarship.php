<?php

namespace App\Models;

class PersonStarship extends BaseModel
{
    protected $guarded = [];

    public function starship()
    {
        return $this->belongsTo(Starship::class);
    }
}
