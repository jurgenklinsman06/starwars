<?php
namespace App\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class VehicleCollection extends JsonResource
{
    public function toArray($request)
    {
        return [
            'name' => $this->vehicle->name,
            'model' => $this->vehicle->model,
            'cons_in_credits' => $this->vehicle->cost_in_credits
        ];
    }
}
