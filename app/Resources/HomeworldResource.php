<?php
namespace App\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class HomeworldResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'population' => $this->population,
            'climate' => $this->climate
        ];
    }
}
