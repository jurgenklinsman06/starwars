<?php
namespace App\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PersonResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'gender' => $this->gender,
            'homeworld' => HomeworldResource::make($this->whenLoaded('homeworld')),
            'vehicles' => count($this->vehicles)? VehicleCollection::collection($this->vehicles) : '-',
            'starships' => count($this->starships)? StarshipCollection::collection($this->starships) : '-'
        ];
    }
}
