<?php
namespace App\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StarshipCollection extends JsonResource
{
    public function toArray($request)
    {
        return [
            'name' => $this->starship->name,
            'model' => $this->starship->model,
            'cons_in_credits' => $this->starship->cost_in_credits,
            'starship_class' => $this->starship->starship_class,
            'manufacturer' => $this->starship->manufacturer
        ];
    }
}
