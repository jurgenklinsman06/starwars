<?php
namespace App\Services;

use App\Models\Homeworld;
use App\Models\Person;
use App\Models\PersonStarship;
use App\Models\PersonVehicle;
use App\Models\Starship;
use App\Models\Vehicle;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Client\Pool;

class PersonService
{
    public function createRequest(string $url, array $params = [])
    {
        $response = Http::get($url, $params);

        if ($response->failed()) {
            $response->throw();
        }

        return $response->object();
    }

    public function createRequestPools(array $urls = [])
    {
        $responses = Http::pool(function (Pool $pool) use ($urls) {
            foreach ($urls as $key => $url) {
                $pools [] = $pool->as($key)->get($url);
            }
            return $pools;
        });

        return $responses;
    }

    public function insertPerson(object $data)
    {
        $personId = $this->getLastPathSegment($data->url);
        $person = Person::find($personId);

        if (null === $person) {
            $personInsert = new Person();
            $personInsert->id = $personId;
            $personInsert->name = $data->name;
            $personInsert->gender = $data->gender;
            $personInsert->homeworld_id = $this->getLastPathSegment($data->homeworld);
            $personInsert->save();
            
            return;
        }
        $person->name = $data->name;
        $person->gender = $data->gender;
        $person->homeworld_id = $this->getLastPathSegment($data->homeworld);
        $person->save();

        return;
    }

    public function insertHomeworld($url = null)
    {
        if (null == $url) {
            return;
        }

        $dataHomeWorld = $this->createRequest($url);

        Homeworld::updateOrCreate(
            ['id' => $this->getLastPathSegment($dataHomeWorld->url)],
            [
                'name' => $dataHomeWorld->name,
                'population' => $dataHomeWorld->population,
                'climate' => $dataHomeWorld->climate,
            ]
        );

        return;
    }

    public function insertVehicle(array $urls = [], string $urlPerson)
    {
        if (empty($urls)) {
            return;
        }
        $responses = $this->createRequestPools($urls);
        $personId = $this->getLastPathSegment($urlPerson);

        foreach ($responses as $value) {
            $vehicleId = $this->getLastPathSegment($value->object()->url);

            PersonVehicle::updateOrCreate(
                ['person_id' => $personId, 'vehicle_id' => $vehicleId],
                ['person_id' => $personId, 'vehicle_id' => $vehicleId]
            );
            
            Vehicle::updateOrCreate(
                ['id' => $vehicleId],
                [
                    'id' => $vehicleId,
                    'name' => $value->object()->name,
                    'model' => $value->object()->model,
                    'cost_in_credits' => $value->object()->cost_in_credits
                ]
            );
        }

        return;
    }

    public function insertStarShips(array $urls = [], string $urlPerson)
    {
        if (empty($urls)) {
            return;
        }

        $responses = $this->createRequestPools($urls);
        $personId = $this->getLastPathSegment($urlPerson);

        foreach ($responses as $value) {
            $starshipId = $this->getLastPathSegment($value->object()->url);

            PersonStarship::updateOrCreate(
                ['person_id' => $personId, 'starship_id' => $starshipId],
                ['person_id' => $personId, 'starship_id' => $starshipId]
            );

            Starship::updateOrCreate(
                ['id' => $starshipId],
                [
                    'id' => $starshipId,
                    'name' => $value->object()->name,
                    'model' => $value->object()->model,
                    'starship_class' => $value->object()->starship_class,
                    'manufacturer' => $value->object()->manufacturer,
                    'cost_in_credits' => $value->object()->cost_in_credits,
                    'hyperdrive_rating' => $value->object()->hyperdrive_rating,
                ]
            );
        }

        return;
    }

    public function getLastPathSegment($url)
    {
        $path = parse_url($url, PHP_URL_PATH); // to get the path from a whole URL
        $pathUrls = explode('/', trim($path, '/')); // get segments delimited by a slash

        if (substr($path, -1) !== '/') {
            array_pop($pathUrls);
        }
        return end($pathUrls); // get the last segment
    }
}
