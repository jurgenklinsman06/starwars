<?php

namespace App\Http\Controllers;

use App\Models\Person;
use App\Resources\PersonCollection;
use App\Resources\PersonResource;
use App\Services\PersonService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PersonController extends Controller
{
    private $baseURL;

    public function index(Request $request, PersonService $personService)
    {
        DB::beginTransaction();
        try {
            $this->baseURL = env('SWAPI_BASE_URL');
    
            $persons = Person::with([
                        'homeworld',
                        'vehicles.vehicle',
                        'starships.starship',
                    ])->where('name', 'like', '%'.$request->input('search').'%')
                    ->paginate($request->input('per_page', 10));
            
            if (count($persons->items()) == 0) {
                $response = $personService->createRequest($this->baseURL.'/people', [
                    'search'=> $request->input('search'),
                    'page'=> $request->input('page'),
                    ]);
    
                if (isset($response->results) || empty($response->results)) {
                    foreach ($response->results as $person) {
                        $personService->insertPerson($person);
                        $personService->insertHomeworld($person->homeworld);
                        $personService->insertVehicle($person->vehicles, $person->url);
                        $personService->insertStarShips($person->starships, $person->url);
                    }
                }

                $persons = Person::with([
                        'homeworld',
                        'vehicles.vehicle',
                        'starships.starship',
                    ])->where('name', 'like', '%'.$request->input('search').'%')
                    ->paginate($request->input('per_page', 10));
            }

            DB::commit();
            return response()->json(
                new PersonCollection($persons),
                200
            );
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    public function person(int $personId, PersonService $personService)
    {
        try {
            $this->baseURL = env('SWAPI_BASE_URL');
            $person = Person::with([
                    'homeworld',
                    'vehicles.vehicle',
                    'starships.starship',
                    ])->where('id', $personId)
                    ->first();


            if (null == $person) {
                $person = $personService->createRequest($this->baseURL.'/people/'.$personId);

                $personService->insertPerson($person);
                $personService->insertHomeworld($person->homeworld);
                $personService->insertVehicle($person->vehicles, $person->url);
                $personService->insertStarShips($person->starships, $person->url);
                $person = Person::with([
                    'homeworld',
                    'vehicles.vehicle',
                    'starships.starship',
                    ])->where('id', $personId)
                    ->first();
            }
            
            return response()->json(
                new PersonResource($person),
                200
            );
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }
}
