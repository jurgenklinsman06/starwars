# Starwars

## Prerequisites

- php >= 7
- sqlite
- composer
- Lumen 8

## How to install and run

1. Clone this repository and change directory into the project root.
2. Copy file `.env.example` to `.env`
3. Run `composer install`.
4. To prepare database, run `php artisan migrate:fresh`.

## API Documentation

1. For detail person you can use `http://157.245.50.20/persons/{personId`
2. For all detail person you can use `http://157.245.50.20/persons` in this endpoint you can use query param `search` and `page`